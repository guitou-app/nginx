FROM nginx

# COPY sites-available/app.guitou.cm.conf /etc/nginx/sites-available/app.guitou.cm.conf 
# COPY sites-available/api.guitou.cm.conf /etc/nginx/sites-available/api.guitou.cm.conf 

# RUN mkdir /etc/nginx/sites-enabled && \
#     ln -s /etc/nginx/sites-available/app.guitou.cm.conf /etc/nginx/sites-enabled/app.guitou.cm.conf && \
#     ln -s /etc/nginx/sites-available/api.guitou.cm.conf /etc/nginx/sites-enabled/api.guitou.cm.conf

RUN rm /etc/nginx/conf.d/default.conf
COPY conf.d/default.prod.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
EXPOSE 443


